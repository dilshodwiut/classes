// Class based approach
class Typer {
  #history = [];

  get current() {
    return this.#history[this.#history.length - 1];
  }
  set current(newValue) {
    this.#history.push(newValue);
    return newValue;
  }
  undo() {
    this.#history.pop();
    return this.#history[this.#history.length - 1];
  }
}

var myclass = new Typer();

myclass.current; // undefined
myclass.current = {}; // {}
myclass.current = []; // []
myclass.current = true; // true
myclass.current; // true
myclass.undo(); // []
myclass.undo(); // {}
myclass.undo(); // undefined
myclass.current; // undefined
