// A functional way
var typer = (function () {
  var arr = [];

  Function.prototype.undo = function () {
    arr.pop();
    this.prototype.current = arr[arr.length - 1];
    return arr[arr.length - 1];
  };

  Function.prototype.current = function () {
    return arr[arr.length - 1];
  };

  Function.prototype.write = function (newValue) {
    arr.push(newValue);
    return newValue;
  };
})();

typer.current(); // undefined
typer.write("new value"); // "new value"
typer.write({ arr: [] }); // {arr: []}
typer.current(); // {arr: []}
typer.undo(); // "new value"
typer.undo(); // "undefined"
typer.current(); // undefined
