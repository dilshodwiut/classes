// The OG way of handling the problem :)
var useUndo = function useUndo() {
  var arr = [];
  function write(item) {
    arr.push(item);
    return item;
  }
  function undo() {
    arr.pop();
    return arr[arr.length - 1];
  }
  function getCurrent(list) {
    return function () {
      return list[list.length - 1];
    };
  }
  return {
    current: getCurrent(arr),
    do: write,
    undo: undo,
  };
};

var typer = useUndo();

typer.current(); // undefined
typer.do("new value"); // "new value"
typer.do({ arr: [] }); // {arr: []}
typer.current(); // {arr: []}
typer.undo(); // "new value"
typer.undo(); // "undefined"
typer.current(); // undefined
